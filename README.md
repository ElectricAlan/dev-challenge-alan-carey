# README #

This repository, used in conjunction with [NPM](https://www.npmjs.com/), contains all the tools necessary to complete the front-end web developer challenge. (See CHALLENGE.md for more information about the challenge itself.)

## Quick start ##

 - Read the [user interface development style guide](http://www.environment.sa.gov.au/documentation/index.html)
 - Fork this repository
 - Run `npm install` to install all the required dependencies
 - Run `gulp` to compile and preview your work in the `temp` folder
 - Commit your changes and push them to Bitbucket
 - Create a pull request so we can review your work

## Requirements ##

This repository uses [Gulp.js](http://gulpjs.com/) to create optimised assets. Gulp.js requires [Node.js](nodejs.org) to run.

## Installing dependencies ##

All dependencies are managed using Node Package Manager. Run `npm install` to install all dependencies listed in `package.json`

## Using Gulp ##

Gulp uses streams to automate numerous asset optimisation steps. Each group of steps is combined in a task.

The `gulpfile.js` in this repository has tasks for optimising images, [concatenating JavaScript](#markdown-header-javascript-concatenation), compiling Sass, and previewing (and updating) your work with [BrowserSync](http://www.browsersync.io/).

Although it is possible to run these tasks individually, they have been grouped into:

 - `build` - optimise images, concatenate JavaScript and compile Sass
 - `build:watch` - the same as `build`, but it runs every time a file is changed
 - `serve` - start the BrowserSync server

Simply running `gulp` by itself will run these three tasks.

### `temp` vs `dist` ###

By default, Gulp is configured to output all files into the `temp` folder. This is by design.

Once all changes have been merged into the `master` branch (see our [contribution guidelines](#markdown-header-contribution-guidelines)) running `gulp build --production` will regenerate the `dist` folder. This includes additional optimisations, such as CSS minification, JavaScript uglification, and cache fingerprinting (by appending a hash to the filename).

### JavaScript concatenation ###

The JavaScript concatenation task requires more configuration than the other tasks. The `jsList` variable contains an array of objects. Each object contains the output folder (`destination`), the output filename (`filename`) and an array of source files (`source`). Use the `paths` object to create easy to update source and output paths.

For example:

```
#!javascript

jsList = [
    {
        source: [
            paths.src.js + "source-file-1.js",
            paths.src.js + "source-file-2.js"
        ],
        destination: paths.dest.js,
        filename: "concatenated-file.js"
    }
];
```

## Sass ##

All CSS in this repository is generated using [Sass](http://sass-lang.com/). See the [Sass section](http://www.environment.sa.gov.au/documentation/css.html#sass) of the [user interface development style guide](http://www.environment.sa.gov.au/documentation/index.html) for general guidance on using Sass.

The `scss` folder includes four settings partials and a `main.scss` file that can be edited as required. *The `_shared` folder includes base settings and partials that should not be edited.*

Third-party partials, such as those from the [inuitcss](https://github.com/inuitcss/getting-started) are loaded using a package manager, such as [NPM](https://www.npmjs.com/) (preferred) or [Bower](http://bower.io/).

### Inverse Triangle CSS (ITCSS) ###

All compilable `.scss` files should be ordered using Inverse Triangle CSS (ITCSS) to ensure logical ordering and inheritance. All partials should be imported in the following order:

 - Functions - self-contained helper functions (eg colour manipulation)
 - Settings
 - Tools and mixins
 - Generic styling and resets
 - Base styling - applied only to elements, not classes
 - Objects - [Object-Oriented CSS (OOCSS) objects](http://www.environment.sa.gov.au/documentation/css.html#object-oriented-css-oocss-)
 - Components - specific styles that can't be set using OOCSS
 - Trumps - important styles that should override all others

See the [Inverted Triangle CSS section](http://www.environment.sa.gov.au/documentation/css.html#inverted-triangle-css-itcss-) of the [user interface development style guide](http://www.environment.sa.gov.au/documentation/index.html) for more information.

## Contribution guidelines ##

All work must adhere to our [user interface development style guide](http://www.environment.sa.gov.au/documentation).
