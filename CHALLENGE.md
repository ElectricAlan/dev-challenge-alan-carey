# CHALLENGE #

We want to know more about the way you work. We want to know what your strengths and weakness are. We want to know how fussy you are about your code.

And the best way to find this out is to give you a challenge.

## Instructions

 1. Clone this repository
 2. Add an form to the page
 3. Add the necessary styling to the form
 4. Add client-side validation (optional)
 5. Add anything else you think would improve the appearance and/or usability of the page
 6. Commit your changes and push them to Bitbucket
 7. Create a pull request so we can review your work

## The form

The form is a simple request for:

 - The visitor's full name (mandatory)
 - Email address (mandatory)
 - Date of birth (mandatory)
 - Favourite colour:
    - Red
    - Green
    - Blue

### Considerations

 - This is a simple form. It should be easy to use on all types of devices. (Refer to [GOV.UK elements' form elements](http://govuk-elements.herokuapp.com/form-elements) for guidance on good form design.)
 - It must work in IE9 and above.
 - It must follow the styles and advice provided in our [user interface development style guide](http://www.environment.sa.gov.au/documentation).

## Hints and tips

 - Refer to README.md for basic information about the project structure.
 - Refer to the [inuitcss documentation](https://github.com/inuitcss/getting-started), as it is used extensively within this repository (most notably for its responsive grid system).
 - The information collected in this form does not need to be sent anywhere. (This is only a test of your front-end development skills.)
